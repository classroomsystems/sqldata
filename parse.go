package sqldata

import (
	"fmt"
	"strconv"
	"strings"
	"unicode"
)

type parser struct {
	file       string
	line       int
	lex        *lexer
	peekCount  int
	token      [1]item // one-token lookahead for parser
	forPrepare bool    // true when parsing a statement for later use
	shifted    int
	argAliases map[string]int
}

type parsedQuery listNode

// parse parses the query template into a parsedQuery.
func parse(query string, forPrepare bool) (q parsedQuery, err error) {
	p := &parser{
		lex:        lex(query, "", ""),
		forPrepare: forPrepare,
		argAliases: make(map[string]int),
	}
	defer p.recover(&err)
	for p.peek().typ != itemEOF {
		n := p.textOrAction()
		if _, ok := n.(noTopLevel); ok {
			p.errorf("%v found at the top level of a template", n)
		}
		q = append(q, n)
	}
	return
}

// evaluate turns a parsedQuery into a queryVal using
// the given Dialect, result type and argument types.
func (q parsedQuery) evaluate(d *Dialect, resultType interface{}, argTypes []interface{}) (queryVal, error) {
	return listNode(q).evaluate(newEvalContext(d, resultType, argTypes))
}

func (p *parser) errorf(format string, args ...interface{}) {
	panic(locatedError{"unknown", 0, p.lex.lineNumber(), fmt.Sprintf(format, args...)})
}

func (*parser) recover(errp *error) {
	if err := recover(); err != nil {
		*errp = err.(locatedError)
	}
	return
}

// next returns the next token.
func (p *parser) next() item {
	if p.peekCount > 0 {
		p.peekCount--
	} else {
		p.token[0] = p.lex.nextItem()
	}
	return p.token[p.peekCount]
}

// backup backs the input stream up one token.
func (p *parser) backup() {
	p.peekCount++
}

// peek returns but does not consume the next token.
func (p *parser) peek() item {
	if p.peekCount > 0 {
		return p.token[p.peekCount-1]
	}
	p.peekCount = 1
	p.token[0] = p.lex.nextItem()
	return p.token[0]
}

// nextNonSpace returns the next non-space token.
func (p *parser) nextNonSpace() (token item) {
	for {
		token = p.next()
		if token.typ != itemSpace {
			break
		}
	}
	return token
}

// expect consumes the next token and guarantees it has the required type.
// Space tokens are skipped.
func (p *parser) expect(expected itemType, context string) item {
	token := p.nextNonSpace()
	if token.typ != expected {
		p.unexpected(token, context)
	}
	return token
}

// expectOneOf consumes the next token and guarantees it has one of the required types.
// Space tokens are skipped.
func (p *parser) expectOneOf(expected1, expected2 itemType, context string) item {
	token := p.nextNonSpace()
	if token.typ != expected1 && token.typ != expected2 {
		p.unexpected(token, context)
	}
	return token
}

// unexpected complains about the token and terminates processing.
func (p *parser) unexpected(token item, context string) {
	p.errorf("unexpected %s in %s", token, context)
}

// accept consumes the next token if it has the required type.
// Note that expect skips spaces while accept does not.
func (p *parser) accept(expected itemType) (item, bool) {
	token := p.next()
	if token.typ != expected {
		p.backup()
		return token, false
	}
	return token, true
}

// accept consumes the next token if it has one of the required type.
// Note that expectOneOf skips spaces while acceptOneOf does not.
func (p *parser) acceptOneOf(expected1, expected2 itemType) (item, bool) {
	token := p.next()
	if token.typ != expected1 && token.typ != expected2 {
		p.backup()
		return token, false
	}
	return token, true
}

// textOrAction parses SQL text or a template action.
func (p *parser) textOrAction() node {
	switch it := p.next(); it.typ {
	case itemText:
		return textNode(it.val)
	case itemLeftDelim:
		return p.action()
	case itemError:
		p.errorf("%s", it.val)
	default:
		p.errorf("unexpected token %v looking for SQL text or a template action", it)
	}
	return nil
}

// action parses a template action. The left delimiter has already been seen.
func (p *parser) action() node {
	switch it := p.nextNonSpace(); it.typ {
	case itemDot, itemField:
		p.backup()
		return p.columnsAction("")
	case itemParamRef:
		p.backup()
		return p.paramsAction("")
	case itemIdentifier:
		switch it.val {
		case "dialect":
			return p.dialectActions()
		case "else":
			it = p.expectOneOf(itemIdentifier, itemRightDelim, "else action")
			if it.typ == itemRightDelim {
				return elseNode{}
			}
			if it.val != "dialect" {
				p.unexpected(it, "else action")
			}
			return p.dialectAction(true)
		case "end":
			p.expect(itemRightDelim, "end action")
			return endNode{}
		case "getSQL":
			return p.getSQLAction()
		case "table":
			return p.tableRefAction()
		}
		if _, ok := fieldFuncs[it.val]; !ok {
			p.errorf("unknown function: %s", it.val)
		}
		return p.funcAction(it.val)
	case itemError:
		p.errorf("%s", it.val)
	default:
		p.errorf("unexpected token %v in template action", it)
	}
	return nil
}

// columnsAction parses an action referencing destination columns.
// The left delimiter has already been seen, and the next item is
// guaranteed to be either itemDot or itemField.
func (p *parser) columnsAction(funcName string) node {
	var ok bool
	it := p.next()
	n := columnsNode{funcName: funcName}
	if it.val != "." {
		n.refs = []string{it.val[1:]}
		it = p.next()
		for it.typ == itemField {
			n.refs = append(n.refs, it.val[1:])
			it = p.next()
		}
		p.backup()
	}
	if _, ok = p.accept(itemSpace); ok {
		if it, ok = p.accept(itemIdentifier); ok {
			n.alias = it.val
		} else if it, ok = p.acceptOneOf(itemString, itemRawString); ok {
			var err error
			n.expr, err = strconv.Unquote(it.val)
			if err != nil {
				p.errorf("%v", err)
			}
		}
	}
	p.expect(itemRightDelim, "template action")
	return n
}

// paramsAction parses an action referencing input columns.
// The left delimiter has already been seen, and the next item is
// guaranteed to be itemParamRef.
func (p *parser) paramsAction(funcName string) node {
	var err error
	var ok bool
	it := p.next()
	n := paramsNode{funcName: funcName}
	if it.val == "$" {
		p.errorf("missing parameter reference after $")
	}
	if isNumericString(it.val[1:]) {
		n.param, err = strconv.Atoi(it.val[1:])
		if err != nil {
			p.errorf("bad number after $: %v", err)
		}
		// params are 1-indexed in the language, so adjust
		n.param--
		n.param += p.shifted
	} else {
		n.param, ok = p.argAliases[it.val[1:]]
		if !ok {
			if funcName == "" {
				return p.aliasAction(it.val[1:])
			}
			p.errorf("undefined parameter alias: %v", it.val)
		}
	}
	it = p.next()
	for it.typ == itemField {
		n.refs = append(n.refs, it.val[1:])
		it = p.next()
	}
	p.backup()
	// some functions can use a table alias
	if _, ok := p.accept(itemSpace); ok {
		if it, ok = p.accept(itemIdentifier); ok {
			n.alias = it.val
		}
	}
	p.expect(itemRightDelim, "template action")
	return n
}

// aliasAction parses an action defining a parameter alias.
// The new alias name has already been seen.
func (p *parser) aliasAction(name string) node {
	var param int
	var err error
	var ok bool
	it := p.expectOneOf(itemAssign, itemRightDelim, "alias action")
	if it.typ == itemRightDelim {
		p.errorf("undefined parameter alias: $%v", name)
	}
	it = p.nextNonSpace()
	switch it.typ {
	case itemParamRef:
		if isNumericString(it.val[1:]) {
			param, err = strconv.Atoi(it.val[1:])
			if err != nil {
				p.errorf("bad number after $: %v", err)
			}
			// params are 1-indexed in the language, so adjust
			param--
			param += p.shifted
		} else {
			param, ok = p.argAliases[it.val[1:]]
			if !ok {
				p.errorf("undefined parameter alias: %v", it.val)
			}
		}
	case itemIdentifier:
		if it.val != "shift" {
			p.errorf("alias assignment requires a parameter reference or \"shift\"")
		}
		param = p.shifted
		p.shifted++
	default:
		p.errorf("alias assignment requires a parameter reference or \"shift\"")
	}
	p.expect(itemRightDelim, "template action")
	p.argAliases[name] = param
	return textNode("")
}

// funcAction parses a function call.
// The left delimiter and function name have already been seen.
func (p *parser) funcAction(funcName string) node {
	it := p.nextNonSpace()
	p.backup()
	switch it.typ {
	case itemParamRef:
		return p.paramsAction(funcName)
	case itemDot, itemField:
		if funcName == "exprs" {
			return p.columnsAction(funcName)
		}
	}
	p.errorf("%s requires an argument reference", funcName)
	panic("not reached")
}

// getSQLAction parses a getSQL action.
// The left delimiter and the word "getSQL" have already been seen.
func (p *parser) getSQLAction() node {
	if p.forPrepare {
		p.errorf("cannot use getSQL in a prepared statement")
	}
	it := p.nextNonSpace()
	if it.typ != itemParamRef {
		p.errorf("getSQL requires an argument reference")
	}
	p.backup()
	return getSQLNode(p.paramsAction("getSQL").(paramsNode))
}

// tableRefAction parses a table reference
// The left delimiter and word "table" have already been seen.
func (p *parser) tableRefAction() node {
	it := p.expect(itemField, "table reference")
	p.expect(itemRightDelim, "table reference")
	return tableNode(it.val[1:])
}

// dialectActions parses an entire dialect construct
// with else clauses The left delimiter and keyword
// "dialect" have already been seen.
func (p *parser) dialectActions() node {
	n := dialectNodes{}
	d := p.dialectAction(false).(dialectNode)
	hadElse := false
loop:
	for {
		nn := p.textOrAction()
		switch nn := nn.(type) {
		case endNode:
			if !hadElse {
				n.dialects = append(n.dialects, d)
			}
			break loop
		case dialectNode:
			if !nn.isElse {
				p.errorf("can't happen: dialect node parsed incorrectly")
			}
			if hadElse {
				p.errorf("{{else dialect}} cannot appear after {{else}}")
			}
			n.dialects = append(n.dialects, d)
			d = nn
		case elseNode:
			if hadElse {
				p.errorf("{{else}} cannot appear after {{else}}")
			}
			n.dialects = append(n.dialects, d)
			hadElse = true
		default:
			if !hadElse {
				d.list = append(d.list, nn)
			} else {
				n.elseList = append(n.elseList, nn)
			}
		}
	}
	return n
}

// dialectAction parses a single dialect action The
// left delimiter and keyword "dialect" have already
// been seen. If isElse is true, the action is an {{else
// dialect}}.
func (p *parser) dialectAction(isElse bool) node {
	n := dialectNode{isElse: isElse}
	it := p.expectOneOf(itemString, itemRightDelim, "dialect action")
	for it.typ != itemRightDelim {
		name, err := strconv.Unquote(it.val)
		if err != nil {
			p.errorf("%v", err)
		}
		n.names = append(n.names, name)
		it = p.expectOneOf(itemString, itemRightDelim, "dialect action")
	}
	if len(n.names) == 0 {
		p.errorf("a dialect action must specify some dialect")
	}
	return n
}

func isNumericString(s string) bool {
	return strings.TrimFunc(s, unicode.IsDigit) == ""
}
