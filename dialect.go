package sqldata

import (
	"database/sql"
	"fmt"
	"reflect"
	"runtime"
	"strings"
)

// A Dialect hides some of the differences between
// implementations of SQL.
type Dialect struct {
	// Name gives a name to this dialect for use by the
	// {{dialect "name"}} construct. Name is free form and
	// has no particular meaning to this package.
	Name string

	// QuoteString returns the properly-quoted string
	// literal representation of s for use in an SQL query.
	QuoteString func(s string) string

	// QuoteIdentifier returns the properly-quoted identifier form
	// of s for use in an SQL query. Note that the returned
	// string need not actually be quoted if QuoteIdentifier
	// determines that quoting is unnecessary.
	QuoteIdentifier func(s string) string

	// Parameter returns the string used to mark the nth
	// replacable parameter in a query. Parameters are
	// numbered from 1. Parameters are always used in order
	// in the query and each is only referenced once, so
	// dialects that can only reference parameters that
	// way are supported.
	Parameter func(n int) string
}

// DefaultDialect is the default Dialect used by Prepare,
// Exec, Query, QueryRow, and QueryAll.
var DefaultDialect = &Dialect{
	Name:            "default",
	QuoteString:     StandardQuoteString,
	QuoteIdentifier: StandardQuoteIdentifier,
	Parameter:       ParameterQuestion,
}

// Prepare creates a Stmt. It parses the query according
// to the template language defined above and generates
// SQL based on the given result type and argument
// types. resultType should have the same type as a
// single result row, and each argType the type of an
// argument to be passed to the Stmt's Query or Exec
// methods. The generated SQL statement is prepared
// for use with db.
func (d *Dialect) Prepare(db Queryer, query string, resultType interface{}, argTypes ...interface{}) (*Stmt, error) {
	q, err := newQueryVal(query, true, d, resultType, argTypes)
	if err != nil {
		return nil, RelocateError(err)
	}
	stmt, err := db.Prepare(q.sql)
	if err != nil {
		return nil, RelocateError(locatedError{"", 0, 0, err.Error()})
	}
	return &Stmt{stmt, q}, nil
}

// Prepare creates a Stmt. It parses the query according
// to the template language defined above and generates
// SQL based on the given result type and argument
// types. resultType should have the same type as a
// single result row, and each argType the type of an
// argument to be passed to the Stmt's Query or Exec
// methods. The generated SQL statement is prepared
// for use with db.
//
// Prepare is a wrapper around DefaultDialect.Prepare.
func Prepare(db Queryer, query string, resultType interface{}, argTypes ...interface{}) (*Stmt, error) {
	s, err := DefaultDialect.Prepare(db, query, resultType, argTypes...)
	return s, RelocateError(err)
}

// Exec executes a statement with the given arguments
// and returns an sql.Result summarizing the effect of
// the statement.
func (d *Dialect) Exec(db Queryer, query string, args ...interface{}) (sql.Result, error) {
	q, err := newQueryVal(query, false, d, nil, args)
	if err != nil {
		return nil, RelocateError(err)
	}
	a, err := q.makeArgs(args)
	if err != nil {
		return nil, err
	}
	return db.Exec(q.sql, a...)
}

// Exec executes a statement with the given arguments
// and returns an sql.Result summarizing the effect of
// the statement.
//
// Exec is a wrapper around DefaultDialect.Exec.
func Exec(db Queryer, query string, args ...interface{}) (sql.Result, error) {
	r, err := DefaultDialect.Exec(db, query, args...)
	return r, RelocateError(err)
}

// Query parses query according to the template language
// defined above and executes it on db for the given
// result type and arguments. Since the argument values
// are available, the getSQL action may be used in the
// query.
func (d *Dialect) Query(db Queryer, query string, resultType interface{}, args ...interface{}) (*Rows, error) {
	q, err := newQueryVal(query, false, d, resultType, args)
	if err != nil {
		return nil, RelocateError(err)
	}
	a, err := q.makeArgs(args)
	if err != nil {
		return nil, err
	}
	return q.rows(db.Query(q.sql, a...))
}

// Query parses query according to the template language
// defined above and executes it on db for the given
// result type and arguments. Since the argument values
// are available, the getSQL action may be used in the
// query.
//
// Query is a wrapper around DefaultDialect.Query.
func Query(db Queryer, query string, resultType interface{}, args ...interface{}) (*Rows, error) {
	r, err := DefaultDialect.Query(db, query, resultType, args...)
	return r, RelocateError(err)
}

// QueryRow is like Query, but it scans the first result
// row into dest. If the query returns no rows, QueryRow
// returns sql.ErrNoRows. Any result rows beyond the
// first are ignored.
//
// Note that there is no need for a separate Scan
// method, since the destination must be specified for
// query generation.
func (d *Dialect) QueryRow(db Queryer, query string, dest interface{}, args ...interface{}) error {
	rs, err := d.Query(db, query, dest, args...)
	return (&Row{rs, RelocateError(err)}).Scan(dest)
}

// QueryRow is like Query, but it scans the first result
// row into dest. If the query returns no rows, QueryRow
// returns sql.ErrNoRows. Any result rows beyond the
// first are ignored.
//
// Note that there is no need for a separate Scan
// method, since the destination must be specified for
// query generation.
//
// QueryRow is a wrapper around DefaultDialect.QueryRow.
func QueryRow(db Queryer, query string, dest interface{}, args ...interface{}) error {
	rs, err := DefaultDialect.Query(db, query, dest, args...)
	return (&Row{rs, RelocateError(err)}).Scan(dest)
}

// QueryAll is like Query, but retrieves all result rows
// and appends them to dest, which must be a slice.
//
// Note that there is no need for a separate Scan
// method, since the destination must be specified for
// query generation.
func (d *Dialect) QueryAll(db Queryer, query string, dest interface{}, args ...interface{}) error {
	rs, err := d.Query(db, query, sliceType(dest), args...)
	return (&AllRows{rs, RelocateError(err)}).Scan(dest)
}

// QueryAll is like Query, but retrieves all result rows
// and appends them to dest, which must be a slice.
//
// Note that there is no need for a separate Scan
// method, since the destination must be specified for
// query generation.
//
// QueryAll is a wrapper around DefaultDialect.QueryAll.
func QueryAll(db Queryer, query string, dest interface{}, args ...interface{}) error {
	rs, err := DefaultDialect.Query(db, query, sliceType(dest), args...)
	return (&AllRows{rs, RelocateError(err)}).Scan(dest)
}

func sliceType(dest interface{}) interface{} {
	s := deref(reflect.ValueOf(dest))
	if !s.IsValid() || !s.CanSet() {
		return fmt.Errorf("sqldata: can't scan rows into unsettable value %v", s)
	}
	if s.Kind() != reflect.Slice {
		return fmt.Errorf("sqldata: can't scan rows into %v", s.Type())
	}
	return reflect.Zero(s.Type().Elem()).Interface()
}

// StandardQuoteString returns s enclosed in single quote
// characters with any embedded single quotes replaced
// by a pair of single quotes.
// It is assumed that the SQL implementation
// can handle arbitraray UTF-8 in string literals.
func StandardQuoteString(s string) string {
	return "'" + strings.Replace(s, "'", "''", -1) + "'"
}

// StandardQuoteIdentifier returns s enclosed in double quote
// characters with any embedded double quotes replaced
// by a pair of double quotes.
// It is assumed that the SQL implementation
// can handle arbitraray UTF-8 in quoted identifiers.
func StandardQuoteIdentifier(s string) string {
	return "\"" + strings.Replace(s, "\"", "\"\"", -1) + "\""
}

// ParameterQuestion returns "?".
// It is intended for use in Dialects.
func ParameterQuestion(_ int) string {
	return "?"
}

// ParameterDollarN returns "$n" where n is the decimal value of n.
// It is intended for use in Dialects.
func ParameterDollarN(n int) string {
	return fmt.Sprint("$", n)
}

// A ParamMarker uses a Dialect to get the replaceable
// parameter markers for a single query. It is used
// both internally and by SQLers.
type ParamMarker struct {
	dialect *Dialect
	n       int
}

// NewParamMarker returns a zeroed ParamMarker that uses Dialect d.
func NewParamMarker(d *Dialect) *ParamMarker {
	return &ParamMarker{dialect: d}
}

// Next returns the next parameter marker string for p.
func (p *ParamMarker) Next() string {
	p.n++
	return p.dialect.Parameter(p.n)
}

type locatedError struct {
	file         string
	line         int
	templateLine int
	msg          string
}

func (e locatedError) Error() string {
	if e.templateLine == 0 {
		return fmt.Sprintf("sqldata: near %s:%d: %s", e.file, e.line, e.msg)
	}
	return fmt.Sprintf("sqldata: near %s:%d (template line %d): %s", e.file, e.line, e.templateLine, e.msg)
}

// RelocateError changes the reported source location
// of an error produced by this package
// to the site of the caller of the caller of RelocateError.
// Many of the functions in this package return error values
// that try to reference the source location of a query template.
// When wrapping such functions,
// those references can become incorrect.
// RelocateError can help.
//
// Error values not produced by this package
// are returned unchanged.
func RelocateError(e error) error {
	if le, ok := e.(locatedError); ok {
		_, le.file, le.line, _ = runtime.Caller(2)
		return le
	}
	return e
}

// RelocateErrorTo changes the reported source location
// of an error produced by this package
// to the given file and line number.
// Many of the functions in this package return error values
// that try to reference the source location of a query template.
// When wrapping such functions,
// those references can become incorrect.
// RelocateErrorTo can help.
//
// Error values not produced by this package
// are returned unchanged.
func RelocateErrorTo(e error, file string, line int) error {
	if le, ok := e.(locatedError); ok {
		le.file, le.line = file, line
		return le
	}
	return e
}

// ErrorLocation returns the source location
// of an error produced by this package.
// If the location is unknown
// or the error was not produced by this package,
// it returns "unknown", 0.
func ErrorLocation(e error) (file string, line int) {
	if le, ok := e.(locatedError); ok {
		return le.file, le.line
	}
	return "unknown", 0
}
