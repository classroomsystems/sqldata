package sqldata

import (
	"bytes"
	"database/sql"
	"database/sql/driver"
	"fmt"
	"reflect"
	"strings"
	"time"
)

var (
	scannerType = reflect.TypeOf((*sql.Scanner)(nil)).Elem()
	valuerType  = reflect.TypeOf((*driver.Valuer)(nil)).Elem()
	timeType    = reflect.TypeOf(time.Time{})
)

// An evalContext is the context in which a node is evaluated.
type evalContext struct {
	dialect    *Dialect
	p          *ParamMarker
	resultType reflect.Type
	args       []interface{}
	argTypes   []reflect.Type
	alias      string
}

func newEvalContext(dialect *Dialect, resultType interface{}, args []interface{}) *evalContext {
	argTypes := make([]reflect.Type, len(args))
	for i := range args {
		argTypes[i] = reflect.TypeOf(args[i])
	}
	return &evalContext{
		dialect,
		NewParamMarker(dialect),
		reflect.TypeOf(resultType),
		args,
		argTypes,
		"",
	}
}

func (ctx *evalContext) Errorf(format string, args ...interface{}) error {
	return locatedError{"unknown", 0, 0, fmt.Sprintf(format, args...)}
}

// A node represents a portion of a parsed template.
// It can convert itself to a queryVal, given a evalContext.
type node interface {
	fmt.Stringer
	evaluate(*evalContext) (queryVal, error)
}

// noTopLevel is an interface that a node may implement
// to indicate that it cannot appear at the top level
// of a template.
type noTopLevel interface {
	node
	noTopLevel()
}

// A listNode is a list of nodes, shockingly.
type listNode []node

func (l listNode) evaluate(ctx *evalContext) (queryVal, error) {
	var q queryVal
	for _, n := range l {
		part, err := n.evaluate(ctx)
		if err != nil {
			return q, err
		}
		q.sql += part.sql
		if q.resultIndexes == nil {
			q.resultIndexes = part.resultIndexes
		} else {
			q.resultIndexes = append(q.resultIndexes, part.resultIndexes...)
		}
		if q.args == nil {
			q.args = part.args
		} else {
			q.args = append(q.args, part.args...)
		}
	}
	return q, nil
}

func (l listNode) String() string {
	b := new(bytes.Buffer)
	for _, n := range l {
		b.WriteString(n.String())
	}
	return b.String()
}

// A textNode emits literal SQL.
type textNode string

func (a textNode) evaluate(_ *evalContext) (queryVal, error) {
	return queryVal{string(a), nil, nil}, nil
}

func (a textNode) String() string {
	return string(a)
}

// A tableNode emits a table column name, not for direct scanning.
type tableNode string

func (a tableNode) evaluate(ctx *evalContext) (queryVal, error) {
	tbl := ctx.dialect.QuoteIdentifier(string(a))
	if ctx.alias != "" {
		tbl = ctx.dialect.QuoteIdentifier(ctx.alias) + "." + tbl
	}
	return queryVal{tbl, nil, nil}, nil
}

func (a tableNode) String() string {
	return fmt.Sprint("{{table.", string(a), "}}")
}

// A columnsNode emits a list of columns based on
// the structure of the destination data type.
type columnsNode struct {
	funcName string // the function being called on the column reference
	// refs is the string representation of an index into the
	// destination data structure. Dots are implied, so {{.}} will
	// have refs []string{} and {{.Foo.Bar}} will have refs
	// []string{"Foo", "Bar"}.
	refs  []string
	alias string // table alias
	expr  string // literal SQL expression
}

func (a columnsNode) String() string {
	ref := strings.Join(a.refs, ".")
	switch {
	case a.alias == "" && a.expr == "":
		return fmt.Sprintf("{{.%s}}", ref)
	case a.alias == "" && a.expr != "":
		return fmt.Sprintf("{{.%s %q}}", ref, a.expr)
	case a.alias != "" && a.expr == "":
		return fmt.Sprintf("{{.%s %s}}", ref, a.alias)
	case a.alias != "" && a.expr != "":
		// This is illegal, but ...
		return fmt.Sprintf("{{.%s %s %q}}", ref, a.alias, a.expr)
	}
	panic("can't happen")
}

func (a columnsNode) evaluate(ctx *evalContext) (queryVal, error) {
	fns, ok := fieldFuncs[a.funcName]
	if !ok {
		return queryVal{}, ctx.Errorf("unknown function %s in %v", a.funcName, a)
	}
	t := ctx.resultType
	if a.alias != "" && a.expr != "" {
		return queryVal{}, ctx.Errorf("you may not specify both literal SQL and a table alias in %v", a)
	}
	if fns.resultSelector == nil {
		return queryVal{}, ctx.Errorf("%s function may not be used with destination values in %v", a.funcName, a)
	}
	sf, err := refStructField(t, a.refs, a.String())
	if err != nil {
		return queryVal{}, ctx.Errorf("%v", err)
	}
	if a.expr != "" || !structish(sf.Type) || isScanner(sf.Type) {
		// We're scanning a single value, rather than the fields of a struct.
		expr := a.expr
		if expr == "" {
			expr = fieldSQL(ctx.dialect, ctx.p, sf, a.alias, fns.resultSelector, fns.resultExpr)
		}
		if expr == "" {
			return queryVal{}, ctx.Errorf("field name or SQL literal required in %v with type %v", a, sf.Type)
		}
		if !fns.hasResults {
			return queryVal{expr, nil, nil}, nil
		}
		return queryVal{expr, [][]int{sf.Index}, nil}, nil
	}
	exprs, indexes := getFields(ctx.dialect, ctx.p, sf, a.alias, fns.resultSelector, fns.resultExpr)
	if len(exprs) == 0 {
		return queryVal{}, ctx.Errorf("can't load into type with no scannable fields: %v", sf.Type)
	}
	if !fns.hasResults {
		// other functions don't cause us to try to scan results
		indexes = nil
	}
	return queryVal{strings.Join(exprs, ", "), indexes, nil}, nil
}

func isScanner(t reflect.Type) bool {
	return t.Implements(scannerType) || reflect.PtrTo(t).Implements(scannerType)
}

// A paramsNode references input parameters for replacement into a query.
type paramsNode struct {
	funcName string // the function being called on the param reference
	param    int    // input parameter number we're referencing
	// refs is the string representation of an index into the
	// input data structure. {{$1}} will  have refs []string{} and
	// {{$1.Foo.Bar}} will have refs []string{"Foo", "Bar"}.
	refs  []string
	alias string // table alias
}

func (a paramsNode) String() string {
	funcPart := ""
	if a.funcName != "" {
		funcPart = a.funcName + " "
	}
	aliasPart := ""
	if a.alias != "" {
		aliasPart = " " + a.alias
	}
	if len(a.refs) == 0 {
		return fmt.Sprintf("{{%s$%d%s}}", funcPart, a.param+1, aliasPart)
	}
	return fmt.Sprintf("{{%s$%d.%s%s}}", funcPart, a.param+1, strings.Join(a.refs, "."), aliasPart)
}

func (a paramsNode) evaluate(ctx *evalContext) (queryVal, error) {
	fns, ok := fieldFuncs[a.funcName]
	if !ok {
		return queryVal{}, ctx.Errorf("unknown function %s in %v", a.funcName, a)
	}
	if a.param < 0 || a.param >= len(ctx.argTypes) {
		return queryVal{}, ctx.Errorf("there is no parameter %d", a.param+1)
	}
	t := ctx.argTypes[a.param]
	sf, err := refStructField(t, a.refs, a.String())
	if err != nil {
		return queryVal{}, ctx.Errorf("%v", err)
	}
	// put the parameter number into the index
	sf.Index = append([]int{a.param}, sf.Index...)
	if !structish(sf.Type) || sf.Type.Implements(valuerType) {
		// We're using a single value, rather than the fields
		// of a struct. Since we're referencing the field
		// explicitly, we always include it.
		expr := fieldSQL(ctx.dialect, ctx.p, sf, a.alias, fieldAlwaysIncluded, fns.argExpr)
		if expr == "" {
			return queryVal{}, ctx.Errorf("can't get any field argument from %v in %v", sf.Type, a)
		}
		if !fns.usesArgs {
			return queryVal{expr, nil, nil}, nil
		}
		return queryVal{expr, nil, []queryArg{indexArg(sf.Index)}}, nil
	}
	exprs, indexes := getFields(ctx.dialect, ctx.p, sf, a.alias, fns.argSelector, fns.argExpr)
	if len(exprs) == 0 {
		return queryVal{}, ctx.Errorf("can't get field arguments from type with no scannable fields: %v", sf.Type)
	}
	var args []queryArg
	if fns.usesArgs {
		args = make([]queryArg, len(indexes))
		for i := range args {
			args[i] = indexArg(indexes[i])
		}
	}
	return queryVal{strings.Join(exprs, ", "), nil, args}, nil
}

type getSQLNode paramsNode

func (a getSQLNode) String() string {
	return paramsNode(a).String()
}

func (a getSQLNode) evaluate(ctx *evalContext) (queryVal, error) {
	if a.param < 0 || a.param >= len(ctx.argTypes) {
		return queryVal{}, ctx.Errorf("there is no parameter %d", a.param+1)
	}
	t := ctx.argTypes[a.param]
	sf, err := refStructField(t, a.refs, a.String())
	if err != nil {
		return queryVal{}, ctx.Errorf("%v", err)
	}
	var iv interface{}
	if len(sf.Index) == 0 {
		iv = ctx.args[a.param]
	} else {
		v := reflect.ValueOf(ctx.args[a.param])
		if v.Kind() == reflect.Ptr {
			v = v.Elem()
		}
		if v.Kind() != reflect.Struct {
			return queryVal{}, ctx.Errorf("struct required, got %T", ctx.args[a.param])
		}
		iv = v.FieldByIndex(sf.Index).Interface()
	}
	sqler, ok := iv.(SQLer)
	if !ok {
		return queryVal{}, ctx.Errorf("%s needs a parameter that implements the SQLer interface", a.String())
	}
	query, args, err := sqler.SQL(ctx.dialect, ctx.p)
	if err != nil {
		return queryVal{}, ctx.Errorf("SQLer error: %v", err)
	}
	var qa []queryArg
	if len(args) > 0 {
		qa = make([]queryArg, len(args))
		for i := range qa {
			qa[i] = valueArg{args[i]}
		}
	}
	return queryVal{query, nil, qa}, nil
}

type dialectNodes struct {
	dialects []dialectNode
	elseList listNode
}

func (a dialectNodes) String() string {
	b := new(bytes.Buffer)
	for _, d := range a.dialects {
		b.WriteString(d.String())
		for _, n := range d.list {
			b.WriteString(n.String())
		}
	}
	if len(a.elseList) > 0 {
		b.WriteString("{{else}}")
		for _, n := range a.elseList {
			b.WriteString(n.String())
		}
	}
	b.WriteString("{{end}}")
	return b.String()
}

func (a dialectNodes) evaluate(ctx *evalContext) (queryVal, error) {
	for _, d := range a.dialects {
		for _, n := range d.names {
			if ctx.dialect.Name == n {
				return d.list.evaluate(ctx)
			}
		}
	}
	return a.elseList.evaluate(ctx)
}

type dialectNode struct {
	isElse bool
	names  []string
	list   listNode
}

func (a dialectNode) String() string {
	b := new(bytes.Buffer)
	if a.isElse {
		b.WriteString("{{else dialect")
	} else {
		b.WriteString("{{dialect")
	}
	for _, n := range a.names {
		fmt.Fprintf(b, " %q", n)
	}
	b.WriteString("}}")
	return b.String()
}

func (a dialectNode) evaluate(ctx *evalContext) (queryVal, error) {
	return queryVal{}, ctx.Errorf("can't happen: dialectNode evaluated")
}

func (a dialectNode) noTopLevel() {}

type elseNode struct{}

func (a elseNode) String() string { return "{{else}}" }

func (a elseNode) evaluate(ctx *evalContext) (queryVal, error) {
	return queryVal{}, ctx.Errorf("can't happen: elseNode evaluated")
}

func (a elseNode) noTopLevel() {}

type endNode struct{}

func (a endNode) String() string { return "{{end}}" }

func (a endNode) evaluate(ctx *evalContext) (queryVal, error) {
	return queryVal{}, ctx.Errorf("can't happen: endNode evaluated")
}

func (a endNode) noTopLevel() {}

func structish(t reflect.Type) bool {
	// time.Time is a struct, but it's opaque and handled directly by database/sql
	if t == timeType || t.Kind() == reflect.Ptr && t.Elem() == timeType {
		return false
	}
	return t.Kind() == reflect.Struct || t.Kind() == reflect.Ptr && t.Elem().Kind() == reflect.Struct
}

func refStructField(t reflect.Type, refs []string, evalContext string) (reflect.StructField, error) {
	var baseIndex []int
	sf := reflect.StructField{Type: t}
	for _, ref := range refs {
		var ok bool
		if !structish(t) {
			return sf, fmt.Errorf("can't reference field %s of non-struct %v in %s", ref, t, evalContext)
		}
		// Deref one level -- reflect.(Value).FieldByIndex will do so as well.
		if t.Kind() == reflect.Ptr {
			t = t.Elem()
		}
		if sf, ok = t.FieldByName(ref); !ok {
			return sf, fmt.Errorf("no field named %s in %v in %s", ref, t, evalContext)
		}
		baseIndex = append(baseIndex, sf.Index...)
		t = sf.Type
	}
	sf.Index = baseIndex
	sf.Type = t
	return sf, nil
}

type fieldSelectorFunc func(name, options, expr string) bool
type fieldExprFunc func(dialect *Dialect, p *ParamMarker, name, options, expr, alias string) string

// fieldSQL returns the SQL expression generated by exprFunc
// for field sf and the given table alias, if selectorFunc returns true.
func fieldSQL(
	dialect *Dialect,
	p *ParamMarker,
	sf reflect.StructField,
	alias string,
	selectorFunc fieldSelectorFunc,
	exprFunc fieldExprFunc,
) string {
	if sf.PkgPath != "" {
		// Ignore unexported fields.
		return ""
	}
	expr := sf.Tag.Get("sqldataExpr")
	if strings.Contains(expr, leftDelim) {
		pq, err := parse(expr, true)
		if err != nil {
			return fmt.Sprintf("!!BAD EXPR IN %v: %v!!", sf, err.(locatedError).msg)
		}
		q, err := listNode(pq).evaluate(&evalContext{alias: alias, dialect: dialect})
		if err != nil {
			return fmt.Sprintf("!!BAD EXPR IN %v: %v!!", sf, err)
		}
		expr = q.sql
	}
	name, options := parseTag(sf.Tag.Get("sqldata"))
	if name == "" {
		if sf.Anonymous {
			// Only use anonymous fields if they are given a name.
			// TODO(micah): Get smart about this like encoding/json.
			return ""
		}
		name = sf.Name
	}
	if !selectorFunc(name, options, expr) {
		return ""
	}
	return exprFunc(dialect, p, name, options, expr, alias)
}

func getFields(
	dialect *Dialect,
	p *ParamMarker,
	sf reflect.StructField,
	alias string,
	selectorFunc fieldSelectorFunc,
	exprFunc fieldExprFunc,
) ([]string, [][]int) {
	var exprs []string
	var indexes [][]int
	t := sf.Type
	// Slice to a lower cap so append will always allocate and copy below.
	baseIndex := sf.Index[:len(sf.Index):len(sf.Index)]
	// Deref so we're working with a struct.
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		s := fieldSQL(dialect, p, f, alias, selectorFunc, exprFunc)
		if s == "" {
			continue
		}
		idx := append(baseIndex, f.Index...)
		indexes = append(indexes, idx)
		exprs = append(exprs, s)
	}
	return exprs, indexes
}

type funcSet struct {
	resultSelector, argSelector fieldSelectorFunc
	resultExpr, argExpr         fieldExprFunc
	hasResults, usesArgs        bool
}

var fieldFuncs = map[string]funcSet{
	"":                   funcSet{fieldWithNameOrExpr, fieldNotExcluded, fieldExpr, fieldValue, true, true},
	"exprs":              funcSet{fieldWithNameOrExpr, fieldNotExcluded, fieldExpr, fieldValue, false, false},
	"names":              funcSet{fieldWithName, fieldWithName, fieldName, fieldName, false, false},
	"values":             funcSet{nil, fieldNotExcluded, nil, fieldValue, false, true},
	"names=values":       funcSet{nil, fieldWithName, nil, fieldNameValue, false, true},
	"keyNames":           funcSet{keyFieldWithName, keyFieldWithName, fieldName, fieldName, false, false},
	"keyValues":          funcSet{nil, keyFieldWithName, nil, fieldValue, false, true},
	"keyNames=values":    funcSet{nil, keyFieldWithName, nil, fieldNameValue, false, true},
	"nonKeyNames":        funcSet{nonKeyFieldWithName, nonKeyFieldWithName, fieldName, fieldName, false, false},
	"nonKeyValues":       funcSet{nil, nonKeyFieldWithName, nil, fieldValue, false, true},
	"nonKeyNames=values": funcSet{nil, nonKeyFieldWithName, nil, fieldNameValue, false, true},
}

func fieldWithNameOrExpr(name, options, expr string) bool {
	return expr != "" || fieldWithName(name, options, expr)
}

func fieldWithName(name, options, expr string) bool {
	return name != "" && name != "-"
}

func fieldNotExcluded(name, options, expr string) bool {
	return name != "-"
}

func fieldAlwaysIncluded(name, options, expr string) bool {
	return true
}

func keyFieldWithName(name, options, expr string) bool {
	return fieldWithName(name, options, expr) && strings.Contains(options, "key")
}

func nonKeyFieldWithName(name, options, expr string) bool {
	return fieldWithName(name, options, expr) && !strings.Contains(options, "key")
}

func fieldExpr(d *Dialect, p *ParamMarker, name, options, expr, alias string) string {
	if expr != "" {
		// alias has already been substituted into expr
		return expr
	}
	return fieldName(d, p, name, options, expr, alias)
}

func fieldName(d *Dialect, p *ParamMarker, name, options, expr, alias string) string {
	name = d.QuoteIdentifier(name)
	if alias != "" {
		return d.QuoteIdentifier(alias) + "." + name
	}
	return name
}

func fieldValue(d *Dialect, p *ParamMarker, name, options, expr, alias string) string {
	return p.Next()
}

func fieldNameValue(d *Dialect, p *ParamMarker, name, options, expr, alias string) string {
	return fieldName(d, p, name, options, expr, alias) + "=" + p.Next()
}

func parseTag(tag string) (name, options string) {
	s := strings.SplitN(tag, ",", 2)
	switch len(s) {
	case 1:
		return s[0], ""
	case 2:
		return s[0], s[1]
	default:
		return "", ""
	}
}

// IndexArg is an implementation of queryArg that indexes a value from the
// supplied arguments.
type indexArg []int

func (idx indexArg) argValue(args []interface{}) (interface{}, error) {
	if len(idx) == 1 {
		return args[idx[0]], nil
	}
	v := reflect.ValueOf(args[idx[0]])
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	if v.Kind() != reflect.Struct {
		return nil, fmt.Errorf("struct required, got %T", args[idx[0]])
	}
	return v.FieldByIndex(idx[1:]).Interface(), nil

}

// ValueArg is an implementation of queryArg that returns a fixed value.
type valueArg struct {
	val interface{}
}

func (a valueArg) argValue(_ []interface{}) (interface{}, error) {
	return a.val, nil
}
