package sqldata

import (
	"database/sql"
	"fmt"
	"reflect"
	"strings"
	"testing"
	"time"
)

type parseTest struct {
	input        string
	forPrepare   bool
	error        string
	notCanonical bool
}

var parseTests = []parseTest{
	{input: ``},
	{input: `a{{.}}{{. pretty}}{{.Comprehensive}}{{.Collection.Of "SQL"}}{{$1}}{{$1.Template.Language}}{{values $1 in}}canonical form, including the {{dialect "conditional"}}with{{else dialect "form"}}{{table.column}}, and{{else}}; don't forget {{getSQL $1}} and finally{{end}}.`},
	{
		input: `{{$foo := shift}}{{$bar := $foo}}{{$baz := $1}}`,
		// FIXME: aliases are never canonical because they don't have a node type.
		notCanonical: true,
	},
	// parse errors
	{
		input: `{{`,
		error: "unclosed action",
	},
	{
		input: `}}`,
		error: "unmatched right delimiter",
	},
	{
		input: `{{$`,
		error: "missing parameter reference after $",
	},
	{
		input: `{{$foo}}`,
		error: "undefined parameter alias: $foo",
	},
	{
		input: `{{$foo := $bar}}`,
		error: "undefined parameter alias: $bar",
	},
	{
		input: `{{. "`,
		error: "unexpected unterminated quoted string in template action",
	},
	{
		input: `{{salad}}`,
		error: "unknown function: salad",
	},
	{
		input: `{{"salad"}}`,
		error: `unexpected token "\"salad\"" in template action`,
	},
	{
		input: `{{. "\J"}}`,
		error: `invalid syntax`,
	},
	{
		input: `{{$١}}`,
		error: `bad number after $`,
	},
	{
		input: `{{names}}`,
		error: `names requires an argument reference`,
	},
	{
		input: `{{dialect`,
		error: "unexpected unclosed action in dialect action",
	},
	{
		input: `{{dialect foo}}`,
		error: `unexpected "foo" in dialect action`,
	},
	{
		input: `{{dialect fooBarBazQuux}}`,
		error: `unexpected "fooBarBazQ"... in dialect action`,
	},
	{
		input: `{{dialect .}}`,
		error: `unexpected <.> in dialect action`,
	},
	{
		input: `{{dialect "default" foo}}`,
		error: `unexpected "foo" in dialect action`,
	},
	{
		input: `{{dialect "bad\Jquote"}}`,
		error: `invalid syntax`,
	},
	{
		input: `{{dialect "default"}}`,
		error: `unexpected token EOF looking for SQL text or a template action`,
	},
	{
		input: `{{else}}`,
		error: "{{else}} found at the top level of a template",
	},
	{
		input: `{{else dialect}}`,
		error: "a dialect action must specify some dialect",
	},
	{
		input: `{{else dialect "foo"}}`,
		error: `{{else dialect "foo"}} found at the top level of a template`,
	},
	{
		input: `{{else dialect foo}}`,
		error: `unexpected "foo" in dialect action`,
	},
	{
		input: `{{else foo}}`,
		error: `unexpected "foo" in else action`,
	},
	{
		input: `{{end}}`,
		error: "{{end}} found at the top level of a template",
	},
	{
		input: `{{end foo}}`,
		error: `unexpected "foo" in end action`,
	},
	{
		input: `{{dialect}}foo{{end}}`,
		error: `a dialect action must specify some dialect`,
	},
	{
		input: `{{dialect "foo" "bar"}}foo{{else}}baz{{else dialect "default" "baz"}}bar{{end}}`,
		error: `{{else dialect}} cannot appear after {{else}}`,
	},
	{
		input: `{{dialect "foo" "bar"}}foo{{else}}baz{{else}}bar{{end}}`,
		error: `{{else}} cannot appear after {{else}}`,
	},
	{
		input:      `prepared {{getSQL $1}}`,
		forPrepare: true,
		error:      "cannot use getSQL in a prepared statement",
	},
	{
		input: `no argument {{getSQL}}`,
		error: "getSQL requires an argument reference",
	},
	{
		input: `no argument {{getSQL .}}`,
		error: "getSQL requires an argument reference",
	},
}

func TestParse(t *testing.T) {
	for _, test := range parseTests {
		q, err := parse(test.input, test.forPrepare)
		if err != nil {
			e, ok := err.(locatedError)
			if !ok || !strings.Contains(e.msg, test.error) {
				t.Errorf("unexpected parse error for %#q: %s", test.input, err)
			}
			continue
		}
		if test.error != "" {
			t.Errorf("unexpected success for %#q", test.input)
			continue
		}
		if test.notCanonical {
			continue
		}
		out := listNode(q).String()
		if out != test.input {
			t.Errorf("unexpected string output for %#q: %#q", test.input, out)
			continue
		}
	}
}

type evalTest struct {
	input         string
	forPrepare    bool
	dst           interface{}
	args          []interface{}
	error         string
	sql           string
	resultIndexes [][]int
	queryArgs     []queryArg
}

// types for the tests to play with

type Person struct {
	ID      int64 `sqldata:",key"`
	Name    string
	Address string
	City    string
	State   string
	Zip     string
	Phone   string
}

type Post struct {
	ID       int64 `sqldata:",key"`
	PersonID int64 // This Post was made by the Person with this ID.
	PostDate time.Time
	Bizarre  bool `sqldata:"-"` // just to test that it isn't used
	Content  string
}

type PersonLastActive struct {
	Person
	LastActive time.Time
}

type PostSummary struct {
	TotalPosts   int       `sqldataExpr:"count({{table.ID}})"`
	LastPostDate time.Time `sqldataExpr:"max({{table.PostDate}})"`
}

type PersonWithSummary struct {
	Person
	PostSummary
}

type BadExpr struct {
	Thing bool `sqldataExpr:"{{table}}"`
}

type Scanner struct {
	val interface{}
}

func (s *Scanner) Scan(src interface{}) error {
	s.val = src
	return nil
}

type ScannedPerson struct {
	ID   Scanner
	Name Scanner
}

var evalTests = []evalTest{
	{input: ``, sql: ``},
	// basic correct cases
	{
		input:         `select {{. "count(*)"}} from Persons`,
		dst:           int(0),
		sql:           `select count(*) from Persons`,
		resultIndexes: [][]int{nil},
	},
	{
		input: `select {{.}} from Persons`,
		dst:   int(0),
		error: "field name or SQL literal required in {{.}} with type int",
	},
	{
		input: `select {{.}} from Persons`,
		dst:   parseTest{},
		error: "can't load into type with no scannable fields: sqldata.parseTest",
	},
	{
		input:         `select {{. "ID"}} from Persons where Scanner()`,
		dst:           Scanner{},
		sql:           `select ID from Persons where Scanner()`,
		resultIndexes: [][]int{nil},
	},
	{
		input:         `select {{. "ID"}} from Persons where PtrScanner()`,
		dst:           &Scanner{},
		sql:           `select ID from Persons where PtrScanner()`,
		resultIndexes: [][]int{nil},
	},
	{
		input:         `select {{.}} from Persons where ScannedPerson()`,
		dst:           ScannedPerson{},
		sql:           `select "ID", "Name" from Persons where ScannedPerson()`,
		resultIndexes: [][]int{{0}, {1}},
	},
	{
		input:         `select {{.ID}}, {{.Name}} from Persons where ScannedPerson()`,
		dst:           ScannedPerson{},
		sql:           `select "ID", "Name" from Persons where ScannedPerson()`,
		resultIndexes: [][]int{{0}, {1}},
	},
	{
		input:         `select {{.}} from Persons`,
		dst:           Person{},
		sql:           `select "ID", "Name", "Address", "City", "State", "Zip", "Phone" from Persons`,
		resultIndexes: [][]int{{0}, {1}, {2}, {3}, {4}, {5}, {6}},
	},
	{
		input:         `select {{.ID}}, {{.Name}} from Persons`,
		dst:           Person{},
		sql:           `select "ID", "Name" from Persons`,
		resultIndexes: [][]int{{0}, {1}},
	},
	{
		input:         `select {{. p}} from Persons p`,
		dst:           Person{},
		sql:           `select "p"."ID", "p"."Name", "p"."Address", "p"."City", "p"."State", "p"."Zip", "p"."Phone" from Persons p`,
		resultIndexes: [][]int{{0}, {1}, {2}, {3}, {4}, {5}, {6}},
	},
	{
		input:         `select {{.ID p}}, {{.Name p}} from Persons p`,
		dst:           Person{},
		sql:           `select "p"."ID", "p"."Name" from Persons p`,
		resultIndexes: [][]int{{0}, {1}},
	},
	// Posts.PostDate is a structure and Posts.Bizarre is ignored, but that shouldn't cause wierdness.
	{
		input:         `select {{.}} from Posts`,
		dst:           Post{},
		sql:           `select "ID", "PersonID", "PostDate", "Content" from Posts`,
		resultIndexes: [][]int{{0}, {1}, {2}, {4}},
	},
	// We can't select into Posts.Bizarre without an
	// expression, since it's excluded and that makes its
	// name unusable.
	{
		input: `select {{.Bizarre}} from Posts`,
		dst:   Post{},
		error: "field name or SQL literal required in {{.Bizarre}} with type bool",
	},
	// Use embedded structs.
	{
		input: `select {{.Person p}}, {{.LastActive "max(s.PostDate)"}}
			from Persons p join Posts s on (s.PersonID=p.ID)
			group by {{.Person p}}
			order by max(s.PostDate) desc limit 10`,
		dst: PersonLastActive{},
		sql: `select "p"."ID", "p"."Name", "p"."Address", "p"."City", "p"."State", "p"."Zip", "p"."Phone", max(s.PostDate)
			from Persons p join Posts s on (s.PersonID=p.ID)
			group by "p"."ID", "p"."Name", "p"."Address", "p"."City", "p"."State", "p"."Zip", "p"."Phone"
			order by max(s.PostDate) desc limit 10`,
		resultIndexes: [][]int{{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6},
			{1}, {0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}},
	},
	// Implicitly use embedded struct fields.
	{
		input: `select {{.ID p}}, {{.Name p}}, {{.LastActive "max(s.PostDate)"}}
			from Persons p join Posts s on (s.PersonID=p.ID)
			group by {{.ID p}}, {{.Name p}}
			order by max(s.PostDate) desc limit 10`,
		dst: PersonLastActive{},
		sql: `select "p"."ID", "p"."Name", max(s.PostDate)
			from Persons p join Posts s on (s.PersonID=p.ID)
			group by "p"."ID", "p"."Name"
			order by max(s.PostDate) desc limit 10`,
		resultIndexes: [][]int{{0, 0}, {0, 1}, {1}, {0, 0}, {0, 1}},
	},
	// Explicitly use embedded struct fields.
	{
		input: `select {{.Person.ID p}}, {{.Person.Name p}}, {{.LastActive "max(s.PostDate)"}}
			from Persons p join Posts s on (s.PersonID=p.ID)
			group by {{.Person.ID p}}, {{.Person.Name p}}
			order by max(s.PostDate) desc limit 10`,
		dst: PersonLastActive{},
		sql: `select "p"."ID", "p"."Name", max(s.PostDate)
			from Persons p join Posts s on (s.PersonID=p.ID)
			group by "p"."ID", "p"."Name"
			order by max(s.PostDate) desc limit 10`,
		resultIndexes: [][]int{{0, 0}, {0, 1}, {1}, {0, 0}, {0, 1}},
	},
	// sqldataExpr
	{
		input:         `select {{.}} from Posts`,
		dst:           PostSummary{},
		sql:           `select count("ID"), max("PostDate") from Posts`,
		resultIndexes: [][]int{{0}, {1}},
	},
	{
		input: `select {{.}}`,
		dst:   BadExpr{},
		// FIXME: This should error, rather than returning bad SQL.
		sql:           `select !!BAD EXPR IN {Thing  bool sqldataExpr:"{{table}}" 0 [0] false}: unexpected "}}" in table reference!!`,
		resultIndexes: [][]int{{0}},
	},
	{
		input: `select {{.Person p}}, {{.PostSummary s}}
			from Persons p join Posts s on (s.PersonID=p.ID)
			group by {{.Person p}}
			order by {{.PostSummary.LastPostDate s}} desc`,
		dst: PersonWithSummary{},
		sql: `select "p"."ID", "p"."Name", "p"."Address", "p"."City", "p"."State", "p"."Zip", "p"."Phone", count("s"."ID"), max("s"."PostDate")
			from Persons p join Posts s on (s.PersonID=p.ID)
			group by "p"."ID", "p"."Name", "p"."Address", "p"."City", "p"."State", "p"."Zip", "p"."Phone"
			order by max("s"."PostDate") desc`,
		resultIndexes: [][]int{{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}, {1, 0}, {1, 1},
			{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}, {1, 1}},
	},
	{
		input:         `select {{exprs .}} from Posts`,
		dst:           PostSummary{},
		sql:           `select count("ID"), max("PostDate") from Posts`,
		resultIndexes: nil,
	},
	{
		input: `select {{.}} from Posts
			UNION select {{exprs .}} from OtherPosts`,
		dst: PostSummary{},
		sql: `select count("ID"), max("PostDate") from Posts
			UNION select count("ID"), max("PostDate") from OtherPosts`,
		resultIndexes: [][]int{{0}, {1}},
	},
	// Basic argument
	{
		input:         `select {{.}} from Persons where City = {{$1}}`,
		dst:           Person{},
		args:          []interface{}{""},
		sql:           `select "ID", "Name", "Address", "City", "State", "Zip", "Phone" from Persons where City = $1`,
		resultIndexes: [][]int{{0}, {1}, {2}, {3}, {4}, {5}, {6}},
		queryArgs:     []queryArg{indexArg{0}},
	},
	{
		input: `{{$1}}`,
		args:  nil,
		error: "there is no parameter 1",
	},
	{
		input: `{{$0}}`,
		args:  []interface{}{""},
		error: "there is no parameter 0",
	},
	{
		input:     `{{$foo := $1}}{{$foo}}`,
		args:      []interface{}{""},
		sql:       "$1",
		queryArgs: []queryArg{indexArg{0}},
	},
	{
		input:     `{{$foo := $1}}{{$foo}}, {{$1}}`,
		args:      []interface{}{""},
		sql:       "$1, $2",
		queryArgs: []queryArg{indexArg{0}, indexArg{0}},
	},
	{
		input:     `{{$foo := shift}}{{$foo}}, {{$1}}`,
		args:      []interface{}{"", ""},
		sql:       "$1, $2",
		queryArgs: []queryArg{indexArg{0}, indexArg{1}},
	},
	{
		input: `{{$foo := shift}}{{$foo}}, {{$1}}, {{$2}}`,
		args:  []interface{}{"", ""},
		error: "there is no parameter 3", // FIXME??
	},
	// Args in structure
	{
		input:         `select {{.}} from Persons where City = {{$1.City}} and State = {{$1.State}}`,
		dst:           Person{},
		args:          []interface{}{Person{}},
		sql:           `select "ID", "Name", "Address", "City", "State", "Zip", "Phone" from Persons where City = $1 and State = $2`,
		resultIndexes: [][]int{{0}, {1}, {2}, {3}, {4}, {5}, {6}},
		queryArgs:     []queryArg{indexArg{0, 3}, indexArg{0, 4}},
	},
	{
		// Should be able to use arguments explicitly, even if
		// they are marked to be ignored.
		input:         `select {{. "ID"}} from Posts where Bizarre = {{$1.Bizarre}}`,
		dst:           int64(0),
		args:          []interface{}{Post{}},
		sql:           `select ID from Posts where Bizarre = $1`,
		resultIndexes: [][]int{nil},
		queryArgs:     []queryArg{indexArg{0, 3}},
	},
	{
		input: `insert into Persons ({{nonKeyNames $1}}) values ({{nonKeyValues $1}})`,
		args:  []interface{}{Person{}},
		sql:   `insert into Persons ("Name", "Address", "City", "State", "Zip", "Phone") values ($1, $2, $3, $4, $5, $6)`,
		queryArgs: []queryArg{
			indexArg{0, 1}, indexArg{0, 2}, indexArg{0, 3},
			indexArg{0, 4}, indexArg{0, 5}, indexArg{0, 6},
		},
	},
	{
		input: `update Persons set {{names=values $2}} where ID={{$1}}`,
		args:  []interface{}{int64(0), Person{}},
		sql:   `update Persons set "ID"=$1, "Name"=$2, "Address"=$3, "City"=$4, "State"=$5, "Zip"=$6, "Phone"=$7 where ID=$8`,
		queryArgs: []queryArg{
			indexArg{1, 0}, indexArg{1, 1}, indexArg{1, 2},
			indexArg{1, 3}, indexArg{1, 4}, indexArg{1, 5},
			indexArg{1, 6}, indexArg{0},
		},
	},
	// Args with alias
	{
		input: `update Persons p, Posts
			set {{nonKeyNames=values $1 p}}
			where Posts.PersonID=p.ID and Posts.ID={{$2}}`,
		args: []interface{}{Person{}, int64(0)},
		sql: `update Persons p, Posts
			set "p"."Name"=$1, "p"."Address"=$2, "p"."City"=$3, "p"."State"=$4, "p"."Zip"=$5, "p"."Phone"=$6
			where Posts.PersonID=p.ID and Posts.ID=$7`,
		queryArgs: []queryArg{
			indexArg{0, 1}, indexArg{0, 2}, indexArg{0, 3},
			indexArg{0, 4}, indexArg{0, 5}, indexArg{0, 6},
			indexArg{1},
		},
	},
	// dialect
	{
		input: `{{dialect "default"}}foo{{else}}bar{{end}}`,
		sql:   `foo`,
	},
	{
		input: `{{dialect "default"}}foo{{end}}`,
		sql:   `foo`,
	},
	{
		input: `{{dialect "not default"}}foo{{end}}`,
		sql:   ``,
	},
	{
		input: `{{dialect "not default"}}foo{{else}}bar{{end}}`,
		sql:   `bar`,
	},
	{
		input: `{{dialect "foo" "bar" "default" "baz"}}foo{{else}}bar{{end}}`,
		sql:   `foo`,
	},
	{
		input: `{{dialect "foo" "bar" "not default" "baz"}}foo{{else}}bar{{end}}`,
		sql:   `bar`,
	},
	{
		input: `{{dialect "foo" "bar"}}foo{{else dialect "default" "baz"}}bar{{else}}baz{{end}}`,
		sql:   `bar`,
	},
	{
		input: `{{dialect "foo" "bar"}}foo{{else}}{{dialect "default" "baz"}}bar{{else}}baz{{end}}{{end}}`,
		sql:   `bar`,
	},
	// getSQL
	{
		input: `nil arguments {{getSQL $1}}`,
		args:  nil,
		error: "there is no parameter 1",
	},
	{
		input: `not an SQLer {{getSQL $1}}`,
		args:  []interface{}{"not an SQLer"},
		error: "{{getSQL $1}} needs a parameter that implements the SQLer interface",
	},
	{
		input: `empty ListValues {{getSQL $1}}`,
		args: []interface{}{
			ListValues{[]int{}},
		},
		sql: "empty ListValues (NULL)",
	},
	{
		input: `non-slice ListValues {{getSQL $1}}`,
		args: []interface{}{
			ListValues{0},
		},
		error: "SQLer error: ListValues requires a slice, got 0",
	},
	{
		input: `ListValues []int {{getSQL $1}}`,
		args: []interface{}{
			ListValues{[]int{1, 2, 3}},
		},
		sql: "ListValues []int ($1, $2, $3)",
		queryArgs: []queryArg{
			valueArg{1}, valueArg{2}, valueArg{3},
		},
	},
	// Mixed types are fine.
	{
		input: `ListValues []interface{} {{getSQL $1}}`,
		args: []interface{}{
			ListValues{[]interface{}{1, "two", 3}},
		},
		sql: "ListValues []interface{} ($1, $2, $3)",
		queryArgs: []queryArg{
			valueArg{1}, valueArg{"two"}, valueArg{3},
		},
	},
	{
		input: `TupleValues []interface{} {{getSQL $1}}`,
		args: []interface{}{
			TupleValues{[]interface{}{1, "two", 3}},
		},
		sql: "TupleValues []interface{} ($1), ($2), ($3)",
		queryArgs: []queryArg{
			valueArg{1}, valueArg{"two"}, valueArg{3},
		},
	},
	{
		input: `non-slice TupleValues {{getSQL $1}}`,
		args: []interface{}{
			TupleValues{0},
		},
		error: "SQLer error: TupleValues requires a slice, got 0",
	},
	{
		input: `non-scannable TupleValues {{getSQL $1}}`,
		args: []interface{}{
			TupleValues{[]struct{}{struct{}{}, struct{}{}}},
		},
		error: "SQLer error: Can't get TupleValues for type with no scannable fields struct {}",
	},
	{
		input: `empty TupleValues {{getSQL $1}}`,
		args: []interface{}{
			TupleValues{[]int{}},
		},
		sql: "empty TupleValues ",
	},
	{
		input: `TupleValues []Person {{getSQL $1}}`,
		args: []interface{}{
			TupleValues{[]Person{
				{1, "Name1", "Address1", "City1", "State1", "Zip1", "Phone1"},
				{2, "Name2", "Address2", "City2", "State2", "Zip2", "Phone2"},
			}},
		},
		sql: "TupleValues []Person ($1, $2, $3, $4, $5, $6, $7), ($8, $9, $10, $11, $12, $13, $14)",
		queryArgs: []queryArg{
			valueArg{int64(1)}, valueArg{"Name1"}, valueArg{"Address1"}, valueArg{"City1"}, valueArg{"State1"}, valueArg{"Zip1"}, valueArg{"Phone1"},
			valueArg{int64(2)}, valueArg{"Name2"}, valueArg{"Address2"}, valueArg{"City2"}, valueArg{"State2"}, valueArg{"Zip2"}, valueArg{"Phone2"},
		},
	},
	{
		input: `TupleValues []*Person {{getSQL $1}}`,
		args: []interface{}{
			TupleValues{[]*Person{
				{1, "Name1", "Address1", "City1", "State1", "Zip1", "Phone1"},
				{2, "Name2", "Address2", "City2", "State2", "Zip2", "Phone2"},
			}},
		},
		sql: "TupleValues []*Person ($1, $2, $3, $4, $5, $6, $7), ($8, $9, $10, $11, $12, $13, $14)",
		queryArgs: []queryArg{
			valueArg{int64(1)}, valueArg{"Name1"}, valueArg{"Address1"}, valueArg{"City1"}, valueArg{"State1"}, valueArg{"Zip1"}, valueArg{"Phone1"},
			valueArg{int64(2)}, valueArg{"Name2"}, valueArg{"Address2"}, valueArg{"City2"}, valueArg{"State2"}, valueArg{"Zip2"}, valueArg{"Phone2"},
		},
	},
	{
		input: `TupleKeyValues []Person {{getSQL $1}}`,
		args: []interface{}{
			TupleKeyValues{[]Person{
				{1, "Name1", "Address1", "City1", "State1", "Zip1", "Phone1"},
				{2, "Name2", "Address2", "City2", "State2", "Zip2", "Phone2"},
			}},
		},
		sql: "TupleKeyValues []Person ($1), ($2)",
		queryArgs: []queryArg{
			valueArg{int64(1)},
			valueArg{int64(2)},
		},
	},
	{
		input: `TupleNonKeyValues []Person {{getSQL $1}}`,
		args: []interface{}{
			TupleNonKeyValues{[]Person{
				{1, "Name1", "Address1", "City1", "State1", "Zip1", "Phone1"},
				{2, "Name2", "Address2", "City2", "State2", "Zip2", "Phone2"},
			}},
		},
		sql: "TupleNonKeyValues []Person ($1, $2, $3, $4, $5, $6), ($7, $8, $9, $10, $11, $12)",
		queryArgs: []queryArg{
			valueArg{"Name1"}, valueArg{"Address1"}, valueArg{"City1"}, valueArg{"State1"}, valueArg{"Zip1"}, valueArg{"Phone1"},
			valueArg{"Name2"}, valueArg{"Address2"}, valueArg{"City2"}, valueArg{"State2"}, valueArg{"Zip2"}, valueArg{"Phone2"},
		},
	},
}

func TestEval(t *testing.T) {
	for _, test := range evalTests {
		q, err := parse(test.input, test.forPrepare)
		if err != nil {
			// All expected parse errors are handled by TestParse above.
			t.Errorf("unexpected parse error for %#q: %s", test.input, err)
			continue
		}

		testDialect := &Dialect{
			Name:            "default",
			QuoteString:     StandardQuoteString,
			QuoteIdentifier: StandardQuoteIdentifier,
			Parameter:       ParameterDollarN, // This is more exacting, so use it for tests.
		}
		qp, err := q.evaluate(testDialect, test.dst, test.args)
		if err != nil {
			e, ok := err.(locatedError)
			if !ok || e.msg != test.error {
				t.Errorf("unexpected execution error for %#q: %s", test.input, err)
			}
			continue
		}
		if test.error != "" {
			t.Errorf("unexpected success for %#q", test.input)
			continue
		}
		if qp.sql != test.sql {
			t.Errorf("unexpected sql for %#q: %#q", test.input, qp.sql)
			continue
		}
		if !reflect.DeepEqual(qp.resultIndexes, test.resultIndexes) {
			t.Errorf("incorrect resultIndexes for %#q: %#v", test.input, qp.resultIndexes)
			continue
		}
		if !reflect.DeepEqual(qp.args, test.queryArgs) {
			t.Errorf("incorrect queryArgs for %#q: %#v", test.input, qp.args)
			continue
		}
	}
}

func TestLookAhead(t *testing.T) {
	p := &parser{lex: lex("{{}}", "", "")}
	it := p.next()
	if it.typ != itemLeftDelim {
		t.Fatal("unexpected item", it)
	}
	p.backup()
	it = p.peek()
	if it.typ != itemLeftDelim {
		t.Fatal("unexpected item", it)
	}
	it = p.next()
	if it.typ != itemLeftDelim {
		t.Fatal("unexpected item", it)
	}
	it = p.next()
	if it.typ != itemRightDelim {
		t.Fatal("unexpected item", it)
	}
}

func TestLocatedErrors(t *testing.T) {
	const badTemplate = "{{else}}"
	const badQuery = "create error"
	atInit1, atInit2 := new(AtInit), new(AtInit)
	var errs [14]error
	atInit1.Prepare(badTemplate, nil)
	atInit2.Prepare(badQuery, nil)
	errs[0] = atInit1.Init(errorQueryer{})
	errs[1] = atInit2.Init(errorQueryer{})
	_, errs[2] = Exec(errorQueryer{}, badTemplate)
	_, errs[3] = Prepare(errorQueryer{}, badTemplate, nil)
	_, errs[4] = Prepare(errorQueryer{}, badQuery, nil)
	_, errs[5] = Query(errorQueryer{}, badTemplate, nil)
	errs[6] = QueryAll(errorQueryer{}, badTemplate, nil)
	errs[7] = QueryRow(errorQueryer{}, badTemplate, nil)
	_, errs[8] = DefaultDialect.Exec(errorQueryer{}, badTemplate)
	_, errs[9] = DefaultDialect.Prepare(errorQueryer{}, badTemplate, nil)
	_, errs[10] = DefaultDialect.Prepare(errorQueryer{}, badQuery, nil)
	_, errs[11] = DefaultDialect.Query(errorQueryer{}, badTemplate, nil)
	errs[12] = DefaultDialect.QueryAll(errorQueryer{}, badTemplate, nil)
	errs[13] = DefaultDialect.QueryRow(errorQueryer{}, badTemplate, nil)
	var baseline int
	for i := range errs {
		err, ok := errs[i].(locatedError)
		if !ok {
			t.Error("unexpected error", i, errs[i])
			continue
		}
		if !strings.HasSuffix(err.file, "/parse_test.go") {
			t.Error("unexpected err.file", i, err.file)
		}
		offset := 0
		switch i {
		case 0:
			baseline = err.line // atInit1.Prepare
		case 1:
			// atInit2.Prepare is before the main sequence
		default:
			offset = 2 // atInit1.Prepare is 2 lines before the main sequence.
		}
		if err.line != baseline+offset+i {
			t.Error("unexpected err.line", i, "got", err.line, "expecting", baseline+offset+i)
		}
	}
}

type errorQueryer struct{}

var _ Queryer = errorQueryer{}

func (errorQueryer) Exec(query string, args ...interface{}) (sql.Result, error) {
	return nil, fmt.Errorf("errorQueryer")
}
func (errorQueryer) Prepare(query string) (*sql.Stmt, error) {
	return nil, fmt.Errorf("errorQueryer")
}
func (errorQueryer) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return nil, fmt.Errorf("errorQueryer")
}
