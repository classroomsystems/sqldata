package sqldata

import "database/sql"

// Stmt is a prepared statement. Stmt is safe for concurrent use by multiple goroutines.
type Stmt struct {
	stmt *sql.Stmt
	queryVal
}

// Stmts are made with Dialect.Prepare, see dialect.go.

// Close closes the statement.
func (s *Stmt) Close() error {
	return s.stmt.Close()
}

// Tx returns a transaction-specific prepared statement from an existing statement.
func (s *Stmt) Tx(tx *sql.Tx) *Stmt {
	return &Stmt{tx.Stmt(s.stmt), s.queryVal}
}

// Exec executes a prepared statement with the given arguments and
// returns a sql.Result summarizing the effect of the statement.
func (s *Stmt) Exec(args ...interface{}) (sql.Result, error) {
	a, err := s.makeArgs(args)
	if err != nil {
		return nil, err
	}
	return s.stmt.Exec(a...)
}

// Query executes a prepared query statement with the given arguments
// and returns the query results as a *Rows.
//
// The argument types must match those passed to Prepare.
func (s *Stmt) Query(args ...interface{}) (*Rows, error) {
	a, err := s.makeArgs(args)
	if err != nil {
		return nil, err
	}
	return s.rows(s.stmt.Query(a...))
}

// QueryAll executes a prepared query statement with the given
// arguments. If an error occurs during the execution of the statement,
// that error will be returned by a call to Scan on the returned *AllRows,
// which is always non-nil.
//
// The argument types must match those passed to Prepare.
func (s *Stmt) QueryAll(args ...interface{}) *AllRows {
	rs, err := s.Query(args...)
	return &AllRows{rs, err}
}

// QueryRow executes a prepared query statement with the given
// arguments. If an error occurs during the execution of the statement,
// that error will be returned by a call to Scan on the returned *Row,
// which is always non-nil. If the query selects no rows, the *Row's
// Scan will return sql.ErrNoRows. Otherwise, the *Row's Scan scans the
// first selected row and discards the rest.
//
// The argument types must match those passed to Prepare.
func (s *Stmt) QueryRow(args ...interface{}) *Row {
	rs, err := s.Query(args...)
	return &Row{rs, err}
}
