Package sqldata was an experiment in connecting SQL queries with Go data structures.
=============================================================

This package is not an ORM, but meets a similar need. Its performance
is competitive with using database/sql directly, at least if you
use prepared statements. It provides conveniences for writing
queries, passing arguments, and scanning results based on data
structures.

This packages is FROZEN and should probably not be used in new work.
Everything here is functional,
but after using this system in production for two years,
I see too many warts.
The template language is too specialized and too difficult to extend.
The SQLer concept is not quite right.

On the other hand, I like using this package
better than any other database access method I've used – in Go or otherwise.
I just think it needs another iteration.
I have much of the design of the next iteration done,
but I haven't had time to finish implementing it yet.
It will not be API-compatible with this package.

I do not intend to make any further changes to this package.
When I have the replacement ready, I'll reference it here.
If anyone besides me is using this, please contact me.

Installation
------------

	go get bitbucket.org/classroomsystems/sqldata

Usage
-----

Usage is intended to be much like database/sql. See godoc for
details.

[![GoDoc](https://godoc.org/bitbucket.org/classroomsystems/sqldata?status.png)](https://godoc.org/bitbucket.org/classroomsystems/sqldata)

Example
-------

```go
	type Person struct {
		ID    int64
		Name  string
		City  string
		State string
	}
	var Person p
	err := sqldata.QueryRow(db, `select {{.}} from Persons where ID={{$1}}`, &p, 5)
	if err != nil { panic(err) }
```

License
-------

Copyright (c) 2015 Classroom Systems, LLC

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
