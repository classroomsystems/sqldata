package sqldata

import (
	"database/sql"
	"testing"
	
	_ "github.com/mattn/go-sqlite3"
)

func TestDialect(t *testing.T) {
	// FIXME: this is pathetic.
	if DefaultDialect.Name != "default" {
		t.Errorf("bad default dialect name")
	}
	s := DefaultDialect.QuoteIdentifier(`"`)
	if s != `""""` {
		t.Errorf("QuoteIdentifier returned %s, expecting %s", s, `""""`)
	}
}

func TestQuerySmoke(t *testing.T) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		t.Fatal("sql.Open:", err)
	}
	tx, err := db.Begin()
	if err != nil {
		t.Fatal("db.Begin:", err)
	}
	rs, err := Query(tx, `select {{. "1"}}`, int64(0))
	if err != nil {
		t.Fatal("Query:", err)
	}
	nrows := 0
	for rs.Next() {
		nrows++
		var n int64
		err = rs.Scan(&n)
		if err != nil {
			t.Error("rs.Scan:", err)
		}
		if n != 1 {
			t.Error("n != 1")
		}
	}
	if nrows != 1 {
		t.Error("nrows != 1")
	}
	if err = rs.Err(); err != nil {
		t.Fatal("rs.Err:", err)
	}
	err = rs.Close()
	if err != nil {
		t.Error("rs.Close:", err)
	}
	err = tx.Commit()
	if err != nil {
		t.Error("tx.Commit:", err)
	}
	err = db.Close()
	if err != nil {
		t.Error("db.Close:", err)
	}
}
