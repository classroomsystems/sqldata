package sqldata

import (
	"context"
	"database/sql"
)

// Queryer abstracts sql.DB, sql.Conn, and sql.Tx.
// It differs from Queryer by using the ...Context methods.
type ContextQueryer interface {
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	PrepareContext(ctx context.Context, query string) (*sql.Stmt, error)
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
}

/*
WithContext returns a Queryer that uses q and ctx behind the scenes.
This allows adapting existing sqldata code to use context without as much editing.

Where you may have used sqldata.Query(db, query)
you now can use sqldata.Query(sqldata.WithContext(ctx, db), query).
*/
func WithContext(ctx context.Context, q ContextQueryer) Queryer {
	return contextQueryer{ctx: ctx, q: q}
}

type contextQueryer struct {
	ctx context.Context
	q   ContextQueryer
}

func (q contextQueryer) Exec(query string, args ...interface{}) (sql.Result, error) {
	return q.q.ExecContext(q.ctx, query, args...)
}

func (q contextQueryer) Prepare(query string) (*sql.Stmt, error) {
	return q.q.PrepareContext(q.ctx, query)
}

func (q contextQueryer) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return q.q.QueryContext(q.ctx, query, args...)
}

var _ Queryer = contextQueryer{}
