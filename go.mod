module bitbucket.org/classroomsystems/sqldata

go 1.14

require github.com/mattn/go-sqlite3 v2.0.2+incompatible
