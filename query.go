package sqldata

import (
	"database/sql"
	"fmt"
	"reflect"
)

// A Queryer can prepare and execute database/sql
// queries. It allows methods not to care whether they
// are called with *sql.DB or *sql.Tx.
type Queryer interface {
	Exec(query string, args ...interface{}) (sql.Result, error)
	Prepare(query string) (*sql.Stmt, error)
	Query(query string, args ...interface{}) (*sql.Rows, error)
}

// A queryVal represents a query (or query portion) to be
// prepared and scanned.
type queryVal struct {
	sql string // the actual query
	// resultIndexes holds indexes a la reflect.Value.FieldByIndex
	// mapping result values to fields in the result structure.
	// resultIndexes[n] is the index for result column n.
	// In case there is a single result column to be scanned directly
	// into the destination value, len(resultIndexes[0]) == 0.
	resultIndexes [][]int
	args          []queryArg // arguments to the query
}

// A queryArg produces a single argument for a database/sql query, given the
// arguments to the sqldata query.
type queryArg interface {
	argValue(args []interface{}) (interface{}, error)
}

func newQueryVal(query string, forPrepare bool, d *Dialect, resultType interface{}, argTypes []interface{}) (queryVal, error) {
	parsed, err := parse(query, forPrepare)
	if err != nil {
		return queryVal{}, err
	}
	return parsed.evaluate(d, resultType, argTypes)
}

// MakeArgs converts the arguments passed to one of the Exec or Query methods
// into arguments suitable for one database/sql's Exec or Query methods.
func (q queryVal) makeArgs(args []interface{}) ([]interface{}, error) {
	a := make([]interface{}, len(q.args))
	for i := range q.args {
		var err error
		a[i], err = q.args[i].argValue(args)
		if err != nil {
			return nil, err
		}
	}
	return a, nil
}

func (q queryVal) rows(r *sql.Rows, err error) (*Rows, error) {
	if err != nil {
		return nil, err
	}
	cs, err := r.Columns()
	if err != nil {
		return nil, err
	}
	if len(cs) != len(q.resultIndexes) {
		r.Close()
		return nil, fmt.Errorf("wrong number of result columns: got %d expecting %d", len(cs), len(q.resultIndexes))
	}
	return &Rows{r, q.resultIndexes, make([]interface{}, len(cs))}, nil
}

// Rows is a wrapper around sql.Rows which will scan
// result rows into data structures according to a
// query template's receiver specifications.
type Rows struct {
	rows          *sql.Rows
	resultIndexes [][]int
	scanBuf       []interface{}
}

// Close closes the Rows, preventing further enumeration. If Next
// returns false, the Rows are closed automatically and it will suffice
// to check the result of Err. Close is idempotent and does not affect
// the result of Err.
func (r *Rows) Close() error {
	return r.rows.Close()
}

// Columns returns the column names. Columns returns an error if
// the rows are closed.
func (r *Rows) Columns() ([]string, error) {
	return r.rows.Columns()
}

// Err returns the error, if any, that was encountered during iteration.
// Err may be called after an explicit or implicit Close.
func (r *Rows) Err() error {
	return r.rows.Err()
}

// Next prepares the next result row for reading with the Scan
// method. It returns true on success, false if there is no next result
// row. Every call to Scan, even the first one, must be preceded by a
// call to Next.
func (r *Rows) Next() bool {
	return r.rows.Next()
}

// Scan copies the columns in the current row into the value pointed
// at by dest. Dest must be a pointer to the same type as the destination
// type passed to Prepare.
func (r *Rows) Scan(dest interface{}) error {
	return r.scan(reflect.ValueOf(dest))
}

// scan scans a row into the value v, allocating or dereferencing pointers as needed.
func (r *Rows) scan(v reflect.Value) error {
	v = deref(v)
	if !v.IsValid() || !v.CanSet() {
		return fmt.Errorf("sqldata: can't scan row into unsettable value %v", v)
	}
	for i := range r.scanBuf {
		r.scanBuf[i] = destPtr(v, r.resultIndexes[i])
	}
	return r.rows.Scan(r.scanBuf...)
}

// A Row is returned by Stmt.QueryRow. See Stmt.QueryRow
// and Row.Scan.
type Row struct {
	rows *Rows
	err  error
}

// Scan copies the columns in the Row into the value pointed
// at by dest. Dest must be a pointer to the same type as the destination
// type passed to Prepare.
func (r *Row) Scan(dest interface{}) error {
	if r.err != nil {
		return r.err
	}
	var err error
	defer r.rows.Close()
	if !r.rows.Next() {
		err = r.rows.Err()
		if err != nil {
			return err
		}
		return sql.ErrNoRows
	}
	err = r.rows.Scan(dest)
	if err != nil {
		return err
	}
	// so we don't miss any error returned by Close
	return r.rows.Close()
}

// An AllRows is returned by Stmt.QueryAll. See
// Stmt.QueryAll and AllRows.Scan.
type AllRows struct {
	rows *Rows
	err  error
}

// Scan appends all the result rows in AllRows to the slice pointed
// to by dest. Dest's element type must be the same as (or a pointer
// to) the destination type passed to Prepare.
func (r *AllRows) Scan(dest interface{}) error {
	if r.err != nil {
		return r.err
	}
	s := deref(reflect.ValueOf(dest))
	if !s.IsValid() || !s.CanSet() {
		return fmt.Errorf("sqldata: can't scan rows into unsettable value %v", s)
	}
	if s.Kind() != reflect.Slice {
		return fmt.Errorf("sqldata: can't scan rows into %v", s.Type())
	}
	defer r.rows.Close()
	for r.rows.Next() {
		s.Set(reflect.Append(s, reflect.Zero(s.Type().Elem())))
		dest := s.Index(s.Len() - 1)
		err := r.rows.scan(dest)
		if err != nil {
			// remove the invalid element just appended
			s.Set(s.Slice(0, s.Len()-1))
			return err
		}
	}
	return r.rows.Err()
}

func deref(v reflect.Value) reflect.Value {
	for v.IsValid() && v.Kind() == reflect.Ptr && !v.IsNil() {
		v = v.Elem()
	}
	return v
}

// destPtr returns a pointer to the field within the struct value v referenced by idx.
// If pointers need to be allocated to reach idx, destPtr does so.
func destPtr(v reflect.Value, idx []int) interface{} {
	for i := range idx {
		for v.Kind() == reflect.Ptr {
			if v.IsNil() {
				v.Set(reflect.New(v.Type().Elem()))
			}
			v = v.Elem()
		}
		v = v.FieldByIndex(idx[i : i+1])
	}
	for v.Kind() == reflect.Ptr {
		if v.IsNil() {
			v.Set(reflect.New(v.Type().Elem()))
		}
		v = v.Elem()
	}
	return v.Addr().Interface()
}
