package sqldata

import (
	"bytes"
	"fmt"
	"reflect"
	"strconv"
)

// An SQLer has the SQL method. See the {{getSQL}}
// template action above for more details.
type SQLer interface {
	// SQL returns a portion of an SQL query along with
	// argument values for any positional parameters in this
	// query portion. The Dialect and ParamMarker should be
	// used to generate the SQL fragment as appropriate.
	SQL(d *Dialect, p *ParamMarker) (string, []interface{}, error)
}

// ListValues is an SQLer that produces a parenthesized list of the values in
// Slice. This is useful for queries like
//
// 	r, err := sqldata.Query(db,
//		"select {{.}} from Table where ID in {{getSQL $1}}",
//		Person{},
//		sqldata.ListValues{[]int{1, 2, 3}},
//	)
//
// Note: If the list is empty, the sql will be "(NULL)".
// This often will work as expected for "WHERE foo IN (NULL)",
// but BE CAREFUL! Both "foo IN (NULL)" and "foo NOT IN (NULL)"
// evaluate to NULL in SQL – neither true nor false.
//
// The problem is that SQL has no syntax for an empty list,
// and tricks with subqueries like '(select 0 from dual where 0=1)'
// are database-specific and sometimes problematic.
// See MySQL bug #44339.
// Caveat SQLor.
type ListValues struct {
	Slice interface{}
}

// SQL implements the SQLer interface.
func (l ListValues) SQL(d *Dialect, p *ParamMarker) (string, []interface{}, error) {
	v := reflect.ValueOf(l.Slice)
	if v.Kind() != reflect.Slice {
		return "", nil, fmt.Errorf("ListValues requires a slice, got %#v", l.Slice)
	}
	if v.Len() == 0 {
		// Thank you, SQL, for not having an empty-set literal. :-(
		return "(NULL)", nil, nil
	}
	args := make([]interface{}, v.Len())
	buf := new(bytes.Buffer)
	buf.WriteString("(")
	for i := range args {
		if i > 0 {
			buf.WriteString(", ")
		}
		buf.WriteString(p.Next())
		args[i] = v.Index(i).Interface()
	}
	buf.WriteString(")")
	return buf.String(), args, nil
}

// Int64Values is an SQLer that produces a parenthesized list of the values in
// Slice. This is useful for queries like
//
// 	r, err := sqldata.Query(db,
//		"select {{.}} from Table where ID in {{getSQL $1}}",
//		Person{},
//		sqldata.Int64Values{1, 2, 3},
//	)
//
// Have a care about NOT IN and empty lists.
// Try `IFNULL(foo NOT IN {{getSQL $1}}, TRUE)`.
type Int64Values []int64

func (s Int64Values) SQL(_ *Dialect, _ *ParamMarker) (string, []interface{}, error) {
	if len(s) == 0 {
		// Thank you, SQL, for not having an empty-set literal. ☹
		return "(NULL)", nil, nil
	}
	b := []byte{'('}
	for i := range s {
		if i > 0 {
			b = append(b, ',')
		}
		b = strconv.AppendInt(b, s[i], 10)
	}
	b = append(b, ')')
	return string(b), nil, nil
}

// TupleValues is an SQLer that produces a comma-separated list of
// parenthesized tuple values. This is useful for multi-record inserts like
//
//	r, err := sqldata.Exec(db,
//		"insert into Table ({{names $1}}) values {{getSQL $2}}",
//		Person{},
//		sqldata.TupleValues{[]Person{p1, p2, p3}},
//	)
type TupleValues struct {
	Slice interface{}
}

// SQL implements the SQLer interface.
func (l TupleValues) SQL(d *Dialect, p *ParamMarker) (string, []interface{}, error) {
	return tupleSQL(d, p, l.Slice, "values")
}

// TupleKeyValues is just like TupleValues, except it only uses values that
// would be selected by the keyValues template function.
type TupleKeyValues struct {
	Slice interface{}
}

// SQL implements the SQLer interface.
func (l TupleKeyValues) SQL(d *Dialect, p *ParamMarker) (string, []interface{}, error) {
	return tupleSQL(d, p, l.Slice, "keyValues")
}

// TupleNonKeyValues is just like TupleValues, except it only uses values that
// would be selected by the nonKeyValues template function.
type TupleNonKeyValues struct {
	Slice interface{}
}

// SQL implements the SQLer interface.
func (l TupleNonKeyValues) SQL(d *Dialect, p *ParamMarker) (string, []interface{}, error) {
	return tupleSQL(d, p, l.Slice, "nonKeyValues")
}

func tupleSQL(d *Dialect, p *ParamMarker, slice interface{}, fname string) (string, []interface{}, error) {
	v := reflect.ValueOf(slice)
	if v.Kind() != reflect.Slice {
		return "", nil, fmt.Errorf("TupleValues requires a slice, got %#v", slice)
	}
	if v.Len() == 0 {
		return "", nil, nil
	}
	t := v.Type().Elem()
	if !structish(t) || t.Implements(valuerType) {
		args := make([]interface{}, v.Len())
		buf := new(bytes.Buffer)
		for i := range args {
			if i > 0 {
				buf.WriteString(", ")
			}
			fmt.Fprint(buf, "(", p.Next(), ")")
			args[i] = v.Index(i).Interface()
		}
		return buf.String(), args, nil
	}
	sf := reflect.StructField{Type: t}
	fns := fieldFuncs[fname]
	// We don't directly use the query made by getFields.
	// So we need to give it its own ParamMarker or it will
	// mess up the real one.
	_, indexes := getFields(d, NewParamMarker(d), sf, "", fns.argSelector, fns.argExpr)
	if len(indexes) == 0 {
		return "", nil, fmt.Errorf("Can't get TupleValues for type with no scannable fields %v", t)
	}
	args := make([]interface{}, v.Len()*len(indexes))
	buf := new(bytes.Buffer)
	for i := 0; i < v.Len(); i++ {
		if i > 0 {
			buf.WriteString(", ")
		}
		buf.WriteString("(")
		for j := 0; j < len(indexes); j++ {
			el := v.Index(i)
			if el.Kind() == reflect.Ptr {
				el = el.Elem()
			}
			if j > 0 {
				buf.WriteString(", ")
			}
			buf.WriteString(p.Next())
			args[i*len(indexes)+j] = el.FieldByIndex(indexes[j]).Interface()
		}
		buf.WriteString(")")
	}
	return buf.String(), args, nil
}
