package sqldata

import "runtime"

// An AtInit holds things that should be done at program
// initialization, but not without an open DB and
// knowledge of the SQL dialect. It's mostly useful
// for preparing SQL statements.
type AtInit []func(Queryer, *Dialect) error

// Do appends a function to the list of functions to perform at init.
func (atInit *AtInit) Do(fn func(Queryer, *Dialect) error) {
	*atInit = append(*atInit, fn)
}

// Init calls atInit.InitDialect(db, DefaultDialect).
func (atInit *AtInit) Init(db Queryer) error {
	return atInit.InitDialect(db, DefaultDialect)
}

// InitDialect runs each of the functions that have been set to run at init.
// If any function returns an error, the error is returned and no further
// functions are called.
func (atInit *AtInit) InitDialect(db Queryer, d *Dialect) error {
	funcs := *atInit
	for i := range funcs {
		err := funcs[i](db, d)
		if err != nil {
			return err
		}
	}
	return nil
}

// Prepare schedules the SQL query to be prepared at database initialization.
// The returned *Stmt will be usable after a successful call to atInit.Init().
func (atInit *AtInit) Prepare(query string, resultType interface{}, argTypes ...interface{}) *Stmt {
	_, file, line, _ := runtime.Caller(1)
	stmt := &Stmt{}
	atInit.Do(func(db Queryer, d *Dialect) error {
		s, err := d.Prepare(db, query, resultType, argTypes...)
		if err == nil {
			*stmt = *s
		}
		return RelocateErrorTo(err, file, line)
	})
	return stmt
}
