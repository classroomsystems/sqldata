# Should the errors returned by sqldata always be located errors?

FOR: Package sqldata is different from other packages
in that its errors are often returned far from the site
of the code that caused the error.
The caller of, say, (*Stmt).Query doesn't know the location
of the Prepare that created the Stmt.
The error it returns to its caller cannot point to the query,
and the query is the most likely cause of error.
If the error is located, the query can be found much more quickly.

AGAINST: This only applies to methods like Prepare
where the use is separated from the definition.
When the query is right in the function call returning error,
then it's just as easy to find as any other error.

FOR: OK, but methods like Query that are normally used directly
are used indirectly by querybatches.New().
New() is a kind of pseudo-Prepare.

AGAINST: That's querybatches's problem.

FOR: But querybatches can't really fix it now, can it?

AGAINST: Yes it can.

FOR: I suppose it could by using ErrorLocation
to see if it has a located error and then synthesizing its own if not.
That seems too roundabout.

AGAINST: It doesn't complicate sqldata with things outside its own concern.

FOR: But it isn't really outside its own concern.
Sqldata is already locating almost every error.

AGAINST: Really? Check that.

## Categorized public functions and methods

### Concerned directly with error locations:

* `func ErrorLocation(e error) (file string, line int)`
* `func RelocateError(e error) error`
* `func RelocateErrorTo(e error, file string, line int) error`

### Do not return an error:

* `func ParameterDollarN(n int) string`
* `func ParameterQuestion(_ int) string`
* `func StandardQuoteIdentifier(s string) string`
* `func StandardQuoteString(s string) string`
* `func (atInit *AtInit) Prepare(query string, resultType interface{}, argTypes ...interface{}) *Stmt`
* `func NewParamMarker(d *Dialect) *ParamMarker`
* `func (p *ParamMarker) Next() string`
* `func (r *Rows) Next() bool`

### Always return a locatedError:

* `func Prepare(db Queryer, query string, resultType interface{}, argTypes ...interface{}) (*Stmt, error)`
* `func (d *Dialect) Prepare(db Queryer, query string, resultType interface{}, argTypes ...interface{}) (*Stmt, error)`

### Sometimes return a locatedError:

* `func Exec(db Queryer, query string, args ...interface{}) (sql.Result, error)`
* `func Query(db Queryer, query string, resultType interface{}, args ...interface{}) (*Rows, error)`
* `func QueryAll(db Queryer, query string, dest interface{}, args ...interface{}) error`
* `func QueryRow(db Queryer, query string, dest interface{}, args ...interface{}) error`
* `func (r *AllRows) Scan(dest interface{}) error`
* `func (atInit *AtInit) Do(fn func(Queryer, *Dialect) error)`
* `func (atInit *AtInit) Init(db Queryer) error`
* `func (atInit *AtInit) InitDialect(db Queryer, d *Dialect) error`
* `func (d *Dialect) Exec(db Queryer, query string, args ...interface{}) (sql.Result, error)`
* `func (d *Dialect) Query(db Queryer, query string, resultType interface{}, args ...interface{}) (*Rows, error)`
* `func (d *Dialect) QueryAll(db Queryer, query string, dest interface{}, args ...interface{}) error`
* `func (d *Dialect) QueryRow(db Queryer, query string, dest interface{}, args ...interface{}) error`

### Never return a locatedError:

* `func (l ListValues) SQL(d *Dialect, p *ParamMarker) (string, []interface{}, error)`
* `func (r *Row) Scan(dest interface{}) error`
* `func (r *Rows) Close() error`
* `func (r *Rows) Columns() ([]string, error)`
* `func (r *Rows) Err() error`
* `func (r *Rows) Scan(dest interface{}) error`
* `func (s *Stmt) Close() error`
* `func (s *Stmt) Exec(args ...interface{}) (sql.Result, error)`
* `func (s *Stmt) Query(args ...interface{}) (*Rows, error)`
* `func (s *Stmt) QueryAll(args ...interface{}) *AllRows`
* `func (s *Stmt) QueryRow(args ...interface{}) *Row`
* `func (s *Stmt) Tx(tx *sql.Tx) *Stmt`
* `func (l TupleKeyValues) SQL(d *Dialect, p *ParamMarker) (string, []interface{}, error)`
* `func (l TupleNonKeyValues) SQL(d *Dialect, p *ParamMarker) (string, []interface{}, error)`
* `func (l TupleValues) SQL(d *Dialect, p *ParamMarker) (string, []interface{}, error)`

## Conversation resumed

FOR: OK, so I was wrong.
The rule implemented currently is that errors
that come from Prepare or from parsing the query template are located.
Others are not.

AGAINST: Honestly, sqldata shouldn't locate errors at all,
except in `(*AtInit).Prepare`, which should wrap every error.
It's the only place where errors are completely disconnected from their source.

FOR: No. Errors that are really syntax errors in the query template
should reference the syntax error source as accurately as possible.
This speeds debugging a lot.

AGAINST: Then let's leave it as is.
Template syntax errors are already located.

FOR: OK. But I'm still not completely happy with it.

AGAINST: Agreed.
