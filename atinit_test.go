package sqldata

import (
	"database/sql"
	"testing"
)

func TestAtInitPrepare(t *testing.T) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		t.Fatal("sql.Open:", err)
	}
	defer db.Close()
	atInit := new(AtInit)
	s := atInit.Prepare(
		`select {{. "1"}} where 1={{$1}} and 2={{$2}}`,
		int64(0),
		int64(0),
		int64(0),
	)
	err = atInit.Init(db)
	if err != nil {
		t.Fatal("Init:", err)
	}
	err = s.Close()
	if err != nil {
		t.Fatal("Close:", err)
	}
}
