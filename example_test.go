package sqldata_test

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"bitbucket.org/classroomsystems/sqldata"

	_ "github.com/mattn/go-sqlite3"
)

var db *sql.DB

func init() {
	var err error
	db, err = sql.Open("sqlite3", ":memory:")
	if err != nil {
		log.Fatal(err)
	}
	e := &errExec{q: db}
	e.Exec(`
		create table Persons (
			ID integer not null primary key,
			Name text not null,
			City text not null,
			State text not null
		)
	`)
	e.Exec(`insert into Persons (ID, Name, City, State) values
		(1, "Bilbo", "The Hill", "The Shire"),
		(2, "Bombur", "Under the Mountain", "Lonely Mountain"),
		(3, "Beorn", "Carrock", "Wilderland"),
		(4, "Bard", "Dale", "Desolation of Smaug")`)
	if e.err != nil {
		log.Fatal(e.err)
	}
}

type errExec struct {
	q sqldata.Queryer
	err error
}

func (e *errExec) Exec(query string, args ...interface{}) {
	if e.err != nil {
		return
	}
	_, e.err = sqldata.Exec(e.q, query, args...)
}


func Example_basic() {
	// A Person represents a row from the Persons table in the database.
	type Person struct {
		ID    int64
		Name  string
		City  string
		State string
	}

	// The {{}} notation allows the query writer to insert field expressions
	// and placeholders based on input and output parameter types.
	var (
		personByID, _ = sqldata.Prepare(db,
			`select {{.}} from Persons where ID={{$1}}`,
			Person{}, int64(0))
		peopleNamedLike, _ = sqldata.Prepare(db,
			`select {{.}} from Persons where Name like {{$1}}`,
			Person{}, "")
		insertPerson, _ = sqldata.Prepare(db,
			`insert into Persons ({{names $1}}) values ({{values $1}})`,
			nil, Person{})
		updatePerson, _ = sqldata.Prepare(db,
			`update Persons set {{names=values $2}} where ID={{$1}}`,
			nil, int64(0), Person{})
	)

	var err error
	var p Person
	var ps []Person

	// Insert a Person
	_, err = insertPerson.Exec(Person{5, "William The Conqueror", "London", "England"})
	if err != nil {
		log.Fatal(err)
	}

	// Load the Person with ID 5.
	err = personByID.QueryRow(5).Scan(&p)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(p.Name, "has ID", p.ID)

	// Load a slice of Persons whose name starts with "william".
	// QueryAll scans all result rows into a slice.
	err = peopleNamedLike.QueryAll("william%").Scan(&ps)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("People named William:", len(ps))

	// Change William's primary key.
	oldID := p.ID
	p.ID = 1066
	_, err = updatePerson.Exec(oldID, p)
	if err != nil {
		log.Fatal(err)
	}
	// Output:
	// William The Conqueror has ID 5
	// People named William: 1
}

func Example_sqldataExpr() {
	// First, some structures for our tables.

	// A Person represents a row from the Persons table in the database.
	type Person struct {
		ID    int64
		Name  string
		City  string
		State string
	}

	// A Post represents a row from the Posts table in our database.
	type Post struct {
		ID       int64
		PersonID int64 // This Post was made by the Person with this ID.
		PostDate time.Time
		Content  string
	}

	// PostSummary represents aggregate information about a group of Posts.
	// Note the use of sqldataExpr to specify the aggregate functions we want.
	type PostSummary struct {
		TotalPosts   int       `sqldataExpr:"count({{table.ID}})"`
		LastPostDate time.Time `sqldataExpr:"max({{table.PostDate}})"`
	}

	// This will get the summary info for all Posts in the database.
	globalSummaryLoader, _ := sqldata.Prepare(db,
		`select {{.}} from Posts`, PostSummary{})
	defer globalSummaryLoader.Close()

	// Now we want the same summary data grouped by person.
	type PersonWithSummary struct {
		Person
		PostSummary
	}
	personSummaryLoader, _ := sqldata.Prepare(db,
		`select {{.Person p}}, {{.PostSummary s}}
		from Persons p join Posts s on (s.PersonID=p.ID)
		group by {{.Person p}}
		order by {{.PostSummary.LastPostDate s}} desc`,
		PersonWithSummary{})
	defer personSummaryLoader.Close()

	// And the same summary data with a different grouping.
	type PostsByWeekday struct {
		Weekday int
		PostSummary
	}
	postsByWeekdayLoader, _ := sqldata.Prepare(db,
		`select {{.Weekday "weekday(PostDate)"}}, {{.PostSummary}}
		from Posts group by weekday(PostDate)`,
		PostsByWeekday{})
	defer postsByWeekdayLoader.Close()
}

func ExampleAtInit() {
	// Usually, this will be package scoped.
	var atInit = new(sqldata.AtInit)

	// Someplace else, declare a type.
	type Part struct {
		ID   int64
		Name string
	}

	// And some prepared statements using the type.
	var (
		someRandomQuery = atInit.Prepare(
			`select {{.}} from Parts where ID={{$1}}`,
			Part{},
			int64(0),
		)
		someOtherQuery = atInit.Prepare(
			`select {{.}} from Parts where Name like {{$1}}`,
			Part{},
			"",
		)
	)

	// Later, open the database and initialize everything.
	db, err := sql.Open("driver", "dsn")
	if err != nil {
		log.Fatal(err)
	}
	err = atInit.Init(db)
	if err != nil {
		log.Fatal(err)
	}

	// Then use the prepared statements.
	_ = someRandomQuery.QueryRow(5)
	_ = someOtherQuery.QueryAll("widget")
}

func ExampleQueryAll() {
	// A Person represents a row from the Persons table in the database.
	type Person struct {
		ID    int64
		Name  string
		City  string
		State string
	}
	var ps []Person
	err := sqldata.QueryAll(db, `select {{.}} from Persons where ID < 5 order by ID`, &ps)
	if err != nil {
		log.Fatal(err)
	}
	for i := range ps {
		fmt.Println(ps[i].Name)
	}
	// Output:
	// Bilbo
	// Bombur
	// Beorn
	// Bard
}
